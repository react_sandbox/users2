import React, { useEffect, useReducer } from "react";
import axios from "axios";

import styles from "./Users.module.scss";

import User from "./components/User/User";
import reducer from "../../store/reducer";

const Users = props => {
    const [users, dispatch] = useReducer(reducer, []);

    useEffect(() => {
        if (Array.isArray(users) && !users.length) {
            axios
                .get("https://jsonplaceholder.typicode.com/users")
                .then(({ data: users }) => {
                    dispatch({ type: "USERS_FETCH_SUCCEEDED", users });
                })
                .catch(e => {
                    dispatch({ type: "USERS_FETCH_FAILED" });
                });
        }
    });

    return (
        <div className={styles.users}>
            {users && users.map(u => <User {...u} key={u.id} />)}
        </div>
    );
};

export default Users;
