import React from "react";

import styles from "./User.module.scss";

const User = props => (
    <div className={styles.user}>
        <h3>{props.name || ""}</h3>
        <span>Email: {props.email || ""}</span>
        <span>Website: {props.website || ""}</span>
    </div>
);

export default User;
