import React, { useState, useEffect } from "react";

import styles from "./Me.module.scss";

import Person from "./components/Person/Person";

const Me = props => {
    const [timer, setTimer] = useState(Date.now());
    const [name, setName] = useState("");

    useEffect(() => {
        let interval = setInterval(() => {
            setTimer(Date.now());
            console.log("calling this");
        }, 1000);
        return () => clearInterval(interval);
    }, []);
    return (
        <div className={styles.me}>
            Timer: {timer}
            <input onChange={e => setName(e.target.value)} />
            <Person name={name} />
        </div>
    );
};

export default Me;
